def number_crossword(grid):
    rows = len(grid)
    cols = len(grid[0]) if rows > 0 else 0
    
    def start_across(r, c):
        return grid[r][c] == '' and (c == 0 or grid[r][c-1] == '#')

    def start_down(r, c):
        return grid[r][c] == '' and (r == 0 or grid[r-1][c] == '#')
    
    number = 1
    positions = []
    
    for r in range(rows):
        for c in range(cols):
            if grid[r][c] == '':
                if start_across(r, c) or start_down(r, c):
                    positions.append((r, c, number))
                    number += 1

    return positions

    crossword_grid = [
    ['', '', '', '', '','', '', '', '#','', '', '','', '', ''],
    ['', '#', '', '#', '', '#','', '#','#','#','','#','', '#',''],
    ['', '', '', '', '','','','' '#', '', '', '','', '', ''],
    ['', '#', '', '#', '', '#','', '#','','#','','#','', '#',''],
    ['', '', '', '', '','', '', '','', '', '', '#', '#','#', '#'],
    ['', '#', '', '#', '#','#', '', '#','', '#', '', '#', '','#', ''],
    ]


positions = number_crossword(crossword_grid)
for position in positions:
    print(position)
      
